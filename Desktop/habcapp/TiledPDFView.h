/*
     File: TiledPDFView.h
 Abstract: This view is backed by a CATiledLayer into which the PDF page is rendered into.
  Version: 2
  */

#import <UIKit/UIKit.h>


@interface TiledPDFView : UIView

- (id)initWithFrame:(CGRect)frame scale:(CGFloat)scale;
- (void)setPage:(CGPDFPageRef)newPage;

@end
