//
//  ACBScoreViewController.h
//  Healthy Aging Brain Care Monitor
//
//  Created by Andrew Heideman on 11/23/13.
//  Copyright (c) 2013 MavenSphere. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ACBScoreViewController : UIViewController <UIAlertViewDelegate,UITableViewDelegate,UITableViewDataSource, UITextFieldDelegate>{
    
    NSMutableArray *medList;
    IBOutlet UITableView *medTable;
    IBOutlet UITextField *medField;
    int score;
    
}
@property (strong, nonatomic) IBOutlet UIButton *addButton;
@property (strong, nonatomic) IBOutlet UITableView *medTable;

@property (strong, nonatomic) IBOutlet UIButton *editBtn;

@property (strong, nonatomic) IBOutlet UITextField *medField;

@property (strong, nonatomic) IBOutlet UITableViewCell *cell1;
@property (strong, nonatomic) IBOutlet UILabel *acbScoreLbl;

- (IBAction)ACBInfo:(id)sender;
- (IBAction)addMed:(id)sender;

@end
