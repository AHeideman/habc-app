//
//  BeginAssessViewController.m
//  Healthy Aging Brain Care Monitor
//
//  Created by Andrew Heideman on 11/23/13.
//  Copyright (c) 2013 MavenSphere. All rights reserved.
//

#import "BeginAssessViewController.h"

@interface BeginAssessViewController ()

@end

@implementation BeginAssessViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)showPP:(UIButton *)sender {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Privacy Policy" message:@"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tristique justo vitae placerat commodo. Aenean imperdiet interdum nulla vitae viverra. Praesent felis elit, laoreet tristique leo nec, tempor hendrerit ante. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed gravida, risus a auctor molestie, sem leo cursus dui, at malesuada ante sapien vel libero. Vivamus sagittis cursus erat. Fusce condimentum at nibh in imperdiet. Morbi interdum, nunc lacinia sollicitudin tincidunt, purus massa vestibulum magna, id varius felis quam non risus. Integer dui velit, euismod at quam ac, dictum tincidunt ipsum." delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
    [alert show];
}

- (IBAction)test:(id)sender {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Settings"
                                                    message:@""
                                                   delegate:self
                                          cancelButtonTitle:@"Close"
                                          otherButtonTitles:@"Change Password",@"Privacy Policy",@"Profile",@"Terms of Use",nil];
    [alert show];
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if([title isEqualToString:@"Privacy Policy"])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Privacy Policy" message:@"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tristique justo vitae placerat commodo. Aenean imperdiet interdum nulla vitae viverra. Praesent felis elit, laoreet tristique leo nec, tempor hendrerit ante. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed gravida, risus a auctor molestie, sem leo cursus dui, at malesuada ante sapien vel libero. Vivamus sagittis cursus erat. Fusce condimentum at nibh in imperdiet. Morbi interdum, nunc lacinia sollicitudin tincidunt, purus massa vestibulum magna, id varius felis quam non risus. Integer dui velit, euismod at quam ac, dictum tincidunt ipsum." delegate:self cancelButtonTitle:@"Close" otherButtonTitles:@"Back",nil];
        [alert show];
        
    }
    if([title isEqualToString:@"Back"]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Settings"
                                                     message:@""
                                                    delegate:self
                                           cancelButtonTitle:@"Close"
                                           otherButtonTitles:@"Change Password",@"Privacy Policy",@"Profile",@"Terms of Use",nil];
        [alert show];
    }
    if([title isEqualToString:@"Change Password"]){
        
    }
    if([title isEqualToString:@"Profile"]){
    
    }
    if([title isEqualToString:@"Terms of Use"]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Terms of Use" message:@"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tristique justo vitae placerat commodo. Aenean imperdiet interdum nulla vitae viverra. Praesent felis elit, laoreet tristique leo nec, tempor hendrerit ante. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed gravida, risus a auctor molestie, sem leo cursus dui, at malesuada ante sapien vel libero. Vivamus sagittis cursus erat. Fusce condimentum at nibh in imperdiet. Morbi interdum, nunc lacinia sollicitudin tincidunt, purus massa vestibulum magna, id varius felis quam non risus. Integer dui velit, euismod at quam ac, dictum tincidunt ipsum." delegate:self cancelButtonTitle:@"Close" otherButtonTitles:@"Back",nil];
        [alert show];
        
    }
    
}

@end
