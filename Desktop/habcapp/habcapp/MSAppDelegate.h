//
//  MSAppDelegate.h
//  habcapp
//
//  Created by Andrew Heideman on 11/25/13.
//  Copyright (c) 2013 MavenSphere Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MSAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
