//
//  main.m
//  habcapp
//
//  Created by Andrew Heideman on 11/25/13.
//  Copyright (c) 2013 MavenSphere Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MSAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MSAppDelegate class]));
    }
}
