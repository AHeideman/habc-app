//
//  TabBar.m
//  Healthy Aging Brain Care Monitor
//
//  Created by Andrew Heideman on 11/23/13.
//  Copyright (c) 2013 MavenSphere. All rights reserved.
//

#import "TabBar.h"

@implementation TabBar

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
