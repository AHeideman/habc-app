//
//  Results TabBarItem.h
//  habcapp
//
//  Created by Andrew Heideman on 11/26/13.
//  Copyright (c) 2013 MavenSphere Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Results_TabBarItem : UITabBarItem

@end
