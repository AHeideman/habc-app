//
//  AssessmentViewController.h
//  Healthy Aging Brain Care Monitor
//
//  Created by Andrew Heideman on 11/23/13.
//  Copyright (c) 2013 MavenSphere. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AssessmentViewController : UIViewController <UIAlertViewDelegate>{
    
    int x;
}

@property (strong, nonatomic) IBOutlet UISegmentedControl *seg1;
@property (strong, nonatomic) IBOutlet UISegmentedControl *seg2;
@property (strong, nonatomic) IBOutlet UISegmentedControl *seg3;
@property (strong, nonatomic) IBOutlet UISegmentedControl *seg4;

@property (strong, nonatomic) IBOutlet UISegmentedControl *seg11;
@property (strong, nonatomic) IBOutlet UISegmentedControl *seg21;
@property (strong, nonatomic) IBOutlet UISegmentedControl *seg31;
@property (strong, nonatomic) IBOutlet UISegmentedControl *seg41;

@property (strong, nonatomic) IBOutlet UIButton *forwardBtn;

- (IBAction)seg1change:(id)sender;

- (void) check;

-(IBAction)nextPage;


@end
