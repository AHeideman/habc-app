//
//  Assess2ViewController.h
//  Healthy Aging Brain Care Monitor
//
//  Created by Andrew Heideman on 11/25/13.
//  Copyright (c) 2013 MavenSphere. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Assess2ViewController : UIViewController <UIAlertViewDelegate>
- (IBAction)settings:(id)sender;

@end
