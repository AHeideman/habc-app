//
//  BeginAssessViewController.h
//  Healthy Aging Brain Care Monitor
//
//  Created by Andrew Heideman on 11/23/13.
//  Copyright (c) 2013 MavenSphere. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BeginAssessViewController : UIViewController<UIAlertViewDelegate>

- (IBAction)showPP:(UIButton *)sender;

- (IBAction)test:(id)sender;
@end
