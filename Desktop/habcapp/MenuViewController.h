//
//  MenuViewController.h
//  Healthy Aging Brain Care Monitor
//
//  Created by Andrew Heideman on 11/23/13.
//  Copyright (c) 2013 MavenSphere. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuViewController : UIViewController <UIAlertViewDelegate>


- (IBAction)gotoLogin:(id)sender;

-(IBAction) link;

@end
