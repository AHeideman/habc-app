

#import "ZoomingPDFViewerViewController.h"
#import "PDFScrollView.h"
#import <QuartzCore/QuartzCore.h>

@implementation ZoomingPDFViewerViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    /*
     Open the PDF document, extract the first page, and pass the page to the PDF scroll view.
     */
    
    
    NSURL *pdfURL = [[NSBundle mainBundle] URLForResource:@"1" withExtension:@"pdf"];
    
    CGPDFDocumentRef PDFDocument = CGPDFDocumentCreateWithURL((__bridge CFURLRef)pdfURL);
    
    CGPDFPageRef PDFPage = CGPDFDocumentGetPage(PDFDocument, 1);
    [(PDFScrollView *)self.view setPDFPage:PDFPage];

    CGPDFDocumentRelease(PDFDocument);
}


@end
