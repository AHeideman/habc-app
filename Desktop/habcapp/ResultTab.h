//
//  ResultTab.h
//  Healthy Aging Brain Care Monitor
//
//  Created by Andrew Heideman on 11/23/13.
//  Copyright (c) 2013 MavenSphere. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResultTab : UITabBarItem

-(IBAction)gotoPage:(id)sender;

@end
