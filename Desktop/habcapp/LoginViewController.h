//
//  LoginViewController.h
//  habcapp
//
//  Created by Andrew Heideman on 11/29/13.
//  Copyright (c) 2013 MavenSphere Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController {
    UITextField *loginField;
    UITextField *passwordField;
}

@property (nonatomic, strong) IBOutlet UITextField *loginField;
@property (nonatomic, strong) IBOutlet UITextField *passwordField;

-(IBAction) link;
  

@end
