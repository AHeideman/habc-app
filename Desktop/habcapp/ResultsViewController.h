//
//  ResultsViewController.h
//  Healthy Aging Brain Care Monitor
//
//  Created by Andrew Heideman on 11/23/13.
//  Copyright (c) 2013 MavenSphere. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResultsViewController : UIViewController <UIAlertViewDelegate>

- (IBAction)scoreInfo:(id)sender;

- (IBAction)settings:(id)sender;

@end
