//
//  AssessmentViewController.m
//  Healthy Aging Brain Care Monitor
//
//  Created by Andrew Heideman on 11/23/13.
//  Copyright (c) 2013 MavenSphere. All rights reserved.
//

#import "AssessmentViewController.h"

@interface AssessmentViewController ()

@end

@implementation AssessmentViewController

@synthesize seg1,seg2,seg3,seg11,seg21,seg31,seg4,seg41,forwardBtn;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void) viewDidAppear:(BOOL)animated{
    self.navigationItem.title=@"HABC - M";
    
     if (seg1.selectedSegmentIndex!=-1 && seg2.selectedSegmentIndex!=-1 && seg3.selectedSegmentIndex!=-1 && seg4.selectedSegmentIndex!=-1) {
         forwardBtn.hidden=FALSE;
     }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    x=0;
    
    self.navigationItem.title=@"HABC - M";

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)seg1change:(id)sender {
    
    if (sender==seg1) {
        
        if ((seg1.selectedSegmentIndex==0)) {
            seg1.tintColor=[UIColor colorWithRed:7/255.0f green:142/255.0f blue:58/255.0f alpha:1.0f];
            seg11.tintColor=[UIColor colorWithRed:7/255.0f green:142/255.0f blue:58/255.0f alpha:1.0f];
            seg1.selectedSegmentIndex=0;
            seg11.selectedSegmentIndex=0;
    
            
        }else if ((seg1.selectedSegmentIndex==1)){
            seg1.tintColor=[UIColor yellowColor];
            seg11.tintColor=[UIColor yellowColor];
            seg1.selectedSegmentIndex=1;
            seg11.selectedSegmentIndex=1;
            
        }else if ((seg1.selectedSegmentIndex==2)){
            seg1.tintColor=[UIColor orangeColor];
            seg11.tintColor=[UIColor orangeColor];
            seg1.selectedSegmentIndex=2;
            seg11.selectedSegmentIndex=2;
            
        }else if ((seg1.selectedSegmentIndex==3)){
            seg1.tintColor=[UIColor redColor];
            seg11.tintColor=[UIColor redColor];
            seg1.selectedSegmentIndex=3;
            seg11.selectedSegmentIndex=3;
        }
    }else if(sender==seg11){
        
        if ((seg11.selectedSegmentIndex==0)) {
            seg1.tintColor=[UIColor colorWithRed:7/255.0f green:142/255.0f blue:58/255.0f alpha:1.0f];
            seg11.tintColor=[UIColor colorWithRed:7/255.0f green:142/255.0f blue:58/255.0f alpha:1.0f];
            seg1.selectedSegmentIndex=0;
            seg11.selectedSegmentIndex=0;
            
        }else if ((seg11.selectedSegmentIndex==1)){
            seg1.tintColor=[UIColor yellowColor];
            seg11.tintColor=[UIColor yellowColor];
            seg1.selectedSegmentIndex=1;
            seg11.selectedSegmentIndex=1;
            
        }else if ((seg11.selectedSegmentIndex==2)){
            seg1.tintColor=[UIColor orangeColor];
            seg11.tintColor=[UIColor orangeColor];
            seg1.selectedSegmentIndex=2;
            seg11.selectedSegmentIndex=2;
            
        }else if ((seg11.selectedSegmentIndex==3)){
            seg1.tintColor=[UIColor redColor];
            seg11.tintColor=[UIColor redColor];
            seg1.selectedSegmentIndex=3;
            seg11.selectedSegmentIndex=3;
        
        }
    } else if(sender==seg2) {
        
        if ((seg2.selectedSegmentIndex==0)) {
            seg2.tintColor=[UIColor colorWithRed:7/255.0f green:142/255.0f blue:58/255.0f alpha:1.0f];
            seg21.tintColor=[UIColor colorWithRed:7/255.0f green:142/255.0f blue:58/255.0f alpha:1.0f];
            seg2.selectedSegmentIndex=0;
            seg21.selectedSegmentIndex=0;
            
        }else if ((seg2.selectedSegmentIndex==1)){
            seg2.tintColor=[UIColor yellowColor];
            seg21.tintColor=[UIColor yellowColor];
            seg2.selectedSegmentIndex=1;
            seg21.selectedSegmentIndex=1;
            
        }else if ((seg2.selectedSegmentIndex==2)){
            seg2.tintColor=[UIColor orangeColor];
            seg21.tintColor=[UIColor orangeColor];
            seg2.selectedSegmentIndex=2;
            seg21.selectedSegmentIndex=2;
            
        }else if ((seg2.selectedSegmentIndex==3)){
            seg2.tintColor=[UIColor redColor];
            seg21.tintColor=[UIColor redColor];
            seg2.selectedSegmentIndex=3;
            seg21.selectedSegmentIndex=3;
        }
    } else if(sender==seg21) {
        
        if ((seg21.selectedSegmentIndex==0)) {
            seg2.tintColor=[UIColor colorWithRed:7/255.0f green:142/255.0f blue:58/255.0f alpha:1.0f];
            seg21.tintColor=[UIColor colorWithRed:7/255.0f green:142/255.0f blue:58/255.0f alpha:1.0f];
            seg2.selectedSegmentIndex=0;
            seg21.selectedSegmentIndex=0;
            
        }else if ((seg21.selectedSegmentIndex==1)){
            seg2.tintColor=[UIColor yellowColor];
            seg21.tintColor=[UIColor yellowColor];
            seg2.selectedSegmentIndex=1;
            seg21.selectedSegmentIndex=1;
            
        }else if ((seg21.selectedSegmentIndex==2)){
            seg2.tintColor=[UIColor orangeColor];
            seg21.tintColor=[UIColor orangeColor];
            seg2.selectedSegmentIndex=2;
            seg21.selectedSegmentIndex=2;
            
        }else if ((seg21.selectedSegmentIndex==3)){
            seg2.tintColor=[UIColor redColor];
            seg21.tintColor=[UIColor redColor];
            seg2.selectedSegmentIndex=3;
            seg21.selectedSegmentIndex=3;
        }
    }else if (sender==seg3){
        
        if ((seg3.selectedSegmentIndex==0)) {
            seg3.tintColor=[UIColor colorWithRed:7/255.0f green:142/255.0f blue:58/255.0f alpha:1.0f];
            seg31.tintColor=[UIColor colorWithRed:7/255.0f green:142/255.0f blue:58/255.0f alpha:1.0f];
            seg3.selectedSegmentIndex=0;
            seg31.selectedSegmentIndex=0;
            
        }else if ((seg3.selectedSegmentIndex==1)){
            seg3.tintColor=[UIColor yellowColor];
            seg31.tintColor=[UIColor yellowColor];
            seg3.selectedSegmentIndex=1;
            seg31.selectedSegmentIndex=1;
            
        }else if ((seg3.selectedSegmentIndex==2)){
            seg3.tintColor=[UIColor orangeColor];
            seg31.tintColor=[UIColor orangeColor];
            seg3.selectedSegmentIndex=2;
            seg31.selectedSegmentIndex=2;
            
        }else if ((seg3.selectedSegmentIndex==3)){
            seg3.tintColor=[UIColor redColor];
            seg31.tintColor=[UIColor redColor];
            seg3.selectedSegmentIndex=3;
            seg31.selectedSegmentIndex=3;
        }  
     
} else if(sender==seg31) {
    
    if ((seg31.selectedSegmentIndex==0)) {
        seg3.tintColor=[UIColor colorWithRed:7/255.0f green:142/255.0f blue:58/255.0f alpha:1.0f];
        seg31.tintColor=[UIColor colorWithRed:7/255.0f green:142/255.0f blue:58/255.0f alpha:1.0f];
        seg3.selectedSegmentIndex=0;
        seg31.selectedSegmentIndex=0;
        
    }else if ((seg31.selectedSegmentIndex==1)){
        seg3.tintColor=[UIColor yellowColor];
        seg31.tintColor=[UIColor yellowColor];
        seg3.selectedSegmentIndex=1;
        seg31.selectedSegmentIndex=1;
        
    }else if ((seg31.selectedSegmentIndex==2)){
        seg3.tintColor=[UIColor orangeColor];
        seg31.tintColor=[UIColor orangeColor];
        seg3.selectedSegmentIndex=2;
        seg31.selectedSegmentIndex=2;
        
    }else if ((seg31.selectedSegmentIndex==3)){
        seg3.tintColor=[UIColor redColor];
        seg31.tintColor=[UIColor redColor];
        seg3.selectedSegmentIndex=3;
        seg31.selectedSegmentIndex=3;
    
    }
}
else if (sender==seg4){
    
    if ((seg4.selectedSegmentIndex==0)) {
        seg4.tintColor=[UIColor colorWithRed:7/255.0f green:142/255.0f blue:58/255.0f alpha:1.0f];
        seg41.tintColor=[UIColor colorWithRed:7/255.0f green:142/255.0f blue:58/255.0f alpha:1.0f];
        seg4.selectedSegmentIndex=0;
        seg41.selectedSegmentIndex=0;
        
    }else if ((seg4.selectedSegmentIndex==1)){
        seg4.tintColor=[UIColor yellowColor];
        seg41.tintColor=[UIColor yellowColor];
        seg4.selectedSegmentIndex=1;
        seg41.selectedSegmentIndex=1;
        
    }else if ((seg4.selectedSegmentIndex==2)){
        seg4.tintColor=[UIColor orangeColor];
        seg41.tintColor=[UIColor orangeColor];
        seg4.selectedSegmentIndex=2;
        seg41.selectedSegmentIndex=2;
        
    }else if ((seg4.selectedSegmentIndex==3)){
        seg4.tintColor=[UIColor redColor];
        seg41.tintColor=[UIColor redColor];
        seg4.selectedSegmentIndex=3;
        seg41.selectedSegmentIndex=3;
    }
    
} else if(sender==seg41) {
    
    if ((seg41.selectedSegmentIndex==0)) {
        seg4.tintColor=[UIColor colorWithRed:7/255.0f green:142/255.0f blue:58/255.0f alpha:1.0f];
        seg41.tintColor=[UIColor colorWithRed:7/255.0f green:142/255.0f blue:58/255.0f alpha:1.0f];
        seg4.selectedSegmentIndex=0;
        seg41.selectedSegmentIndex=0;
        
    }else if ((seg41.selectedSegmentIndex==1)){
        seg4.tintColor=[UIColor yellowColor];
        seg41.tintColor=[UIColor yellowColor];
        seg4.selectedSegmentIndex=1;
        seg41.selectedSegmentIndex=1;
        
    }else if ((seg41.selectedSegmentIndex==2)){
        seg4.tintColor=[UIColor orangeColor];
        seg41.tintColor=[UIColor orangeColor];
        seg4.selectedSegmentIndex=2;
        seg41.selectedSegmentIndex=2;
        
    }else if ((seg41.selectedSegmentIndex==3)){
        seg4.tintColor=[UIColor redColor];
        seg41.tintColor=[UIColor redColor];
        seg4.selectedSegmentIndex=3;
        seg41.selectedSegmentIndex=3;
        
    }
    
}
    [self check];
}

-(IBAction)nextPage{
    
    self.navigationItem.title=@" ";
    
    [self performSegueWithIdentifier:@"segue" sender:self];

    
}


-(void)check{
    
    if (seg1.selectedSegmentIndex!=-1 && seg2.selectedSegmentIndex!=-1 && seg3.selectedSegmentIndex!=-1 && seg4.selectedSegmentIndex!=-1) {
        
        self.navigationItem.title=@" ";
        
        [self performSegueWithIdentifier:@"segue" sender:self];
        
        
    }
}




@end


